package b22;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.*;
import org.junit.jupiter.api.*;
 
public class example1 {

	static fact obj;
	@BeforeAll
	public static void abc1() {
		 obj=new fact();
		System.out.println("I am BeforeAll");
	}
	
	@BeforeEach
	public void abc2() {
		System.out.println("I am BeforeEach");
	}

	@Test
	public void abc3() {
		assertEquals(obj.fact(),120);
		System.out.println("I am TestCase");
	}

	
	@Test
	public void abc8() {
		assertEquals(obj.fact(),120);
		System.out.println("I am TestCase");
	}

	@AfterEach
	public void abc4() {
		System.out.println("I am AfterEach");
	}
	
	@AfterAll
	public static void abc5() {
		System.out.println("I am AfterAll");
	}

	
	
}
